<?php 
    $acao   = isset($_GET["acao"]) ? $_GET["acao"] : "Salvar";
    $id     = isset($_GET["id"]) ? $_GET["id"] : NULL;
    

    if($id){
        $valores = consultar("cliente", "WHERE cliente_id = $id");
    }
    
    $nome       = isset($valores[0]["cliente_nome"]) ? $valores[0]["cliente_nome"] : NULL;
    $endereco   = isset($valores[0]["cliente_endereco"]) ? $valores[0]["cliente_endereco"] : NULL;
    $bairro     = isset($valores[0]["cliente_bairro"]) ? $valores[0]["cliente_bairro"] : NULL;
    $cidade     = isset($valores[0]["cliente_cidade"]) ? $valores[0]["cliente_cidade"] : NULL;
    $cep        = isset($valores[0]["cliente_cep"]) ? $valores[0]["cliente_cep"] : NULL;
    $tel        = isset($valores[0]["cliente_tel"]) ? $valores[0]["cliente_tel"] : NULL;
    $email      = isset($valores[0]["cliente_email"]) ? $valores[0]["cliente_email"] : NULL;
    
?>

<div class="base-home">
    <h1 class="titulo"><span class="cor">Novo</span> cadastro</h1>
    <div class="base-formulario">	
        <form action="op/op_cliente.php" method="POST">
            <label>Nome</label>
            <input name="txt_nome" value="<?php echo $nome ?>" type="text" placeholder="Insira seu nome">
            <label>Endereço</label>
            <input name="txt_endereco" value="<?php echo $endereco ?>" type="text" placeholder="Insira seu endereço">
            <label>Bairro</label>
            <input name="txt_bairro" value="<?php echo $bairro ?>" type="text" placeholder="Insira seu bairro">	
            <div class="col">
                <label>Cidade/UF</label>
                <input name="txt_cidade" value="<?php echo $cidade ?>" type="text" placeholder="Insira sua cidade/UF">
            </div>				
            <div class="col">
                <label>CEP</label>
                <input name="txt_cep" value="<?php echo $cep ?>" type="text" placeholder="Insira seu cep">
            </div>
            <div class="col">
                <label>Telefone/Celular</label>
                <input name="txt_tel" value="<?php echo $tel ?>" type="text" placeholder="Insira seu Telefone/Celular">
            </div>				
            <div class="col">
                <label>Email</label>
                <input name="txt_email" value="<?php echo $email ?>" type="text" placeholder="Insira um email ativo">
            </div>			
            <input type="hidden" name="acao" value="<?php echo $acao ?>">
            <input type="hidden" name="id" value="<php echo $id ?>">
            <input type="submit" value="<?php echo $acao ?>" class="btn">
            <input type="reset" name="Reset" id="button" value="Limpar" class="btn limpar">
        </form>
    </div>	
</div>

