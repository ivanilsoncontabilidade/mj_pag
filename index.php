<?php
    require("config/config.php");
    require("config/crud.php");
?>


<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>mjailton</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>

    <body>
        <div class="conteudo">	
            <div class="base-central">	
		
                <?php include "header.php"; ?>
		
		<?php include "nav.php"; ?>		
		
                <?php 
                
                $link = $_GET["link"];
                $p[1] = "home.php";
                $p[2] = "frm/frm_cliente.php";
                $p[3] = "lst/lst_cliente.php";
                
                if(!empty($link)){
                    
                    if(file_exists($p[$link])){
                        include $p[$link];
                    }else{
                        include "home.php";
                    }
                    
                }else{                    
                    include "home.php";                    
                }
                
                ?>
		
                <?php include "footer.php"; ?>
            </div>		
        </div>		
    </body>
</html>
