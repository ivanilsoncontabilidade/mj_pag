<?php
	
    // função: abrir a conexão
    function abrirConexao(){
        $conexao = mysqli_connect(HOSTNAME, USERNAME, PASSWORD, DATABASE) or die(mysqli_connect_error());
        mysqli_set_charset($conexao, CHARSET);		
        return $conexao;
    }
	
    // função: fechar a conexão
    function fecharConexao($conexao){
        mysqli_close($conexao) or die (mysqli_error($conexao));		
    }
	
    // função: executar um comando
    function executar($sql){
        $conexao = abrirConexao();
        $query = mysqli_query($conexao, $sql) or die(@mysqli_error($conexao));
        fecharConexao($conexao);		
        return $query;		
    }
	
    // função: consultar dados
    function consultar($tabela, $where=null, $campos="*"){
        $sql = "SELECT {$campos} FROM {$tabela} {$where}";					
        $query = executar($sql);
        if(!mysqli_num_rows($query)){
            return false;
        }else{
            while($linha = mysqli_fetch_assoc($query)){
                $dados[] = $linha;
            }
            return $dados;
        }
    }
	
    // função: inserir registros
    function inserir($tabela, array $dados){
        $campos = implode(", ", array_keys($dados));
        $valores = "'" . implode("', '", $dados) . "'";
        $sql = "INSERT INTO $tabela ({$campos}) VALUES ({$valores})";		
        return executar($sql);
    }
	
    // função: alterar dados
    function alterar($tabela, array $dados, $condicao){
        foreach($dados as $chave=>$valor)
            $campos[] = "{$chave} = {'$valor}'";
            $campos = implode(", ", $campos);
            $sql = "UPDATE {$tabela} SET {$campos} WHERE {$condicao}";
            return executar($sql);
    }
	
    // função: excluir registro
    function excluir($tabela, $condicao){
        $sql = "DELETE FROM {$tabela} WHERE {$condicao}";
        return executar($ql);
    }
?>

