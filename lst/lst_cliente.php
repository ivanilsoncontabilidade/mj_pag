<?php
    $clientes = consultar('cliente');
?>

<div class="base-home">
    <h1 class="titulo"><span class="cor">Lista de</span> Clientes</h1>
    <div class="base-lista">
        <div class="base-colunas"> <!-- busca-lista -->
            <div class="col">
                <span>Valor</span>
                <input type="text" value="" name="">                
            </div>
            <div class="col">
                <span>Campo</span>
                <select name="">
                    <option>Nome1</option>
                    <option>Nome2</option>
                    <option>Nome3</option>
                </select>
            </div>
            <div class="col">
                <input type="submit" value="Buscar" class="btn">
            </div>
        </div>
        <span class="qtde"><b>18</b> clientes cadastrados</span>
        <div class="tabela"><?php
            if($clientes){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">    
                    <thead>
                        <tr>
                            <th width="25%" align="left">Nome</th>
                            <th width="25%" align="left">Email</th>
                            <th width="10%" align="left">Tel</th>
                            <th width="20%" colspan="2" align="center">Ações</th>
                        </tr>
                    </thead>
                    <tbody><?php                    
                        foreach($clientes as $cliente){?>
                            <tr class="cor1">
                                <td><?php echo $cliente["cliente_nome"]; ?></td>
                                <td><?php echo $cliente["cliente_email"]; ?></td>
                                <td><?php echo $cliente["cliente_tel"]; ?></td>
                                <td align="center">
                                    <a href="index.php?link=2&id=<?php echo $cliente["cliente_id"]?>&acao=Alterar" class="btn">Edita</a>
                                </td>
                                <td align="center">
                                    <a href="index.php?link=2&id=<?php echo $cliente["cliente_id"]?>&acao=Excluir" class="btn excluir">Exclui</a>
                                </td>
                            </tr><?php 
                        }?>
                    </tbody>                
                </table><?php           
            }else{
                echo "Nenhum cliente cadastrado!";
            }?>
        </div>					
        <ul class="paginacao">
            <li><a href="#" class="primeiro">Primeiro</a></li>
            <li><a href="#" class="ant">Anterior</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#" class="prox">Próximo</a></li>
            <li><a href="#" class="ultimo">Ultimo</a></li>
        </ul>
    </div>	
</div>