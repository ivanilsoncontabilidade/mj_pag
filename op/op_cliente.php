<?php

    require("../config/config.php");
    require("../config/crud.php");
    $acao = $_POST["acao"];
    $id = $_POST["id"];
    
    // recebe os dados
    $nome       = filter_input(INPUT_POST, 'txt_nome', FILTER_SANITIZE_STRING);
    $endereco   = filter_input(INPUT_POST, 'txt_endereco', FILTER_SANITIZE_STRING);
    $bairro     = filter_input(INPUT_POST, 'txt_bairro', FILTER_SANITIZE_STRING);
    $cidade     = filter_input(INPUT_POST, 'txt_cidade', FILTER_SANITIZE_STRING);
    $cep        = filter_input(INPUT_POST, 'txt_cep', FILTER_SANITIZE_STRING);
    $tel        = filter_input(INPUT_POST, 'txt_tel', FILTER_SANITIZE_STRING);
    $email      = filter_input(INPUT_POST, 'txt_email', FILTER_SANITIZE_STRING);
    
    echo "Processando as Informações Cadastradas...!<hr>";
    echo $nome . "<br>";
    echo $endereco . "<br>";
    echo $bairro . "<br>";
    echo $cidade . "<br>";
    echo $cep . "<br>";
    echo $tel . "<br>";
    echo $email . "<br>";
    
    $dados = array(
        "cliente_nome"=>$nome,
        "cliente_endereco"=>$endereço,
        "cliente_bairro"=>$bairro,
        "cliente_cidade"=>$cidade,
        "cliente_cep"=>$cep,
        "cliente_tel"=>$tel,
        "cliente_email"=>$email
    );
    
    if($acao=="Salvar"){
        inserir("cliente", $dados);
    }
    
    if($acao=="Alterar"){
        alterar("cliente", $dados, "WHERE cliente_id = $id");
    }

    if($acao=="Excluir"){
        excluir("cliente", "WHERE cliente_id = $id");
    }    
    
    
    
    header("location:../index.php?link=3");

